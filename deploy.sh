docker build -t manuelxaguilar/multi-docker-client:latest -t manuelxaguilar/multi-docker-client:$GIT_SHA -f ./client/Dockerfile ./client
docker build -t manuelxaguilar/multi-docker-server:latest -t manuelxaguilar/multi-docker-server:$GIT_SHA -f ./server/Dockerfile ./server
docker build -t manuelxaguilar/multi-docker-worker:latest -t manuelxaguilar/multi-docker-worker:$GIT_SHA -f ./worker/Dockerfile ./worker
docker push manuelxaguilar/multi-docker-client:latest
docker push manuelxaguilar/multi-docker-server:latest
docker push manuelxaguilar/multi-docker-worker:latest
docker push manuelxaguilar/multi-docker-client:$GIT_SHA
docker push manuelxaguilar/multi-docker-server:$GIT_SHA
docker push manuelxaguilar/multi-docker-worker:$GIT_SHA
kubectl apply -f k8s
kubectl set image deployments/client-deployment client=manuelxaguilar/multi-docker-client:$GIT_SHA
kubectl set image deployments/server-deployment server=manuelxaguilar/multi-docker-server:$GIT_SHA
kubectl set image deployments/server-deployment server=manuelxaguilar/multi-docker-server:$GIT_SHA